App.Status = DS.Model.extend({
	name : DS.attr('string'),
	slug : DS.attr('string'),
	img : DS.attr('string'),
	user : DS.hasMany('App.User')
});

App.Status.FIXTURES = [
{
		"id" : "1",
		"name" : "Disponible",
		"slug" : "online",
		"img" : "img/online.jpg",
		"class" : "iconperso-online",
		"user" : [1]
	},
	{
		"id" : "2",
		"name" : "Occupé",
		"slug" : "busy",
		"img" : "img/busy.png",
		"class" : "iconperso-busy",
		"user" : [2]
	},
	{
		"id" : "3",
		"name" : "Ne pas déranger",
		"slug" : "busy",
		"img" : "img/busy.png",
		"class" : "iconperso-busy",
		"user" : [3]
	},
	{
		"id" : "4",
		"name" : "Absent",
		"slug" : "absent",
		"img" : "img/absent.png",
		"class" : "iconperso-absent",
		"user" : [4]
	},
	{
		"id" : "5",
		"name" : "Hors ligne",
		"slug" : "offline",
		"img" : "img/offline.png",
		"class" : "iconperso-offline",
		"user" : [5]
	}
]
;
