App.ApplicationController = Ember.Controller.extend({	
	init : function(){
		$(window).bind("resize", resizeWindow);
		$('#dropdown-profil').dropdown();
		$('#dropdown-status').dropdown();
		function resizeWindow() {
			var screenWidth = document.documentElement.clientWidth;
			var screenHeight = document.documentElement.clientHeight;
			$('#container-fluid').height(screenHeight - 100);
			$('#row-fluid').height(screenHeight - 100);
			$('#tab-content-contacts').css('max-height', screenHeight - 170);
			$('#container-list-contact').css('max-height', screenHeight - 220);
			$('#container').css('width', screenWidth - 360);
			$('#communications').css('width', screenWidth - 360);
		}	
		setTimeout(function(){
			resizeWindow();
		}, 1);
	},
	
	animateDialerSpeed : 100,
	dialerIsVisible : false,
	openDialer : function(){
		console.log(this.dialerIsVisible);
		if(!this.dialerIsVisible){
			$('#containerDialer').animate({
				height : "210px"
			}, this.animateDialerSpeed, function(){
				$('#dialer').show(this.animateDialerSpeed);
				this.set('dialerIsVisible', true);						
			});
			$('#container-list-contact').animate({
				height : '-=210px'
			}, this.animateDialerSpeed);
			$('#dialing').animate({
				width : '-=20px'
			}, function(){
				$('#closeDialer').show("fade");
			});
		}
	},

	closeDialer : function(){
		$('#dialer').hide(this.animateDialerSpeed, function(){
			$('#containerDialer').animate({
				height : '0px'
			});
			$('#container-list-contact').animate({
				height : '+=210px'
			});
			$('#closeDialer').hide("fade",function(){
				$('#dialing').animate({
					width : '+=20px'
				});
			});	
			this.dialerIsVisible = false;	
		});
	},

	dialNumber : function(value){
		if(parseInt(value) || value == 0){	
			$('#dialing').val($('#dialing').val() + value);			
		}
		if(value == "dialerDeleteButton" || value == "dialerDeleteImage"){
			$('#dialing').val($('#dialing').val().substring(0, $('#dialing').val().length-1));
		}else if(value == "dialerCallButton" || value == "dialerCallImage"){
			//TODO Call
		}	
	}
});
