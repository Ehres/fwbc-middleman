function visioController($scope, $routeParams, $http){	
	$http.get('data/' + $routeParams.userId + '.json').success(function(data){
		$scope.user = data;
		initVisioConf();
	});
}