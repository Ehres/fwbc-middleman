var loaderObj = {

    templates : [
    'contact.html',
    'profile.html',
    'reachability.html',
    'settings.html',
    'visio.html',
    'voicemail.html'
]
};

loadTemplates(loaderObj.templates);
//This function loads all templates into the view
function loadTemplates(templates) {
    $(templates).each(function() {
        var tempObj = $('<script>');
        tempObj.attr('type', 'text/x-handlebars');
        var dataTemplateName = this.substring(0, this.indexOf('.'));
        tempObj.attr('data-template-name', dataTemplateName);
        tempObj.attr('id', dataTemplateName);
        $.ajax({
            async: false,
            type: 'GET',
            url: 'views/' + this,
            success: function(resp) {
                tempObj.html(resp);
                $('body').append(tempObj);                
            }
        });
    });
}




App = Ember.Application.create({LOG_TRANSITIONS: true});

App.Store = DS.Store.extend({
  revision: 12,
  adapter : 'DS.FixtureAdapter'
});

App.Router.map(function(){
	this.resource('reachability');
	this.resource('voicemail');
});





App.ApplicationRoute = Ember.Route.extend({
	setupController: function(controller){
		var users = App.User.find();
		var userConnected = App.User.find(1);
		var listStatus = App.Status.find();
		
		controller.set("users", users);
		controller.set("userConnected", userConnected);
		controller.set("listStatus", listStatus);
	}
});

App.UsersRoute = Ember.Route.extend({
	model : function(){
		return App.User.find();
	}
});