App.User = DS.Model.extend({
	firstname : DS.attr('string'),
	lastname : DS.attr('string'),
	number : DS.attr('string'),
	status : DS.belongsTo('App.Status'),
	mail : DS.attr('string'),

	fullname: function(){
		return this.get('firstname') + ' ' + this.get('lastname');
  	}.property('firstname', 'lastname')
});

App.User.FIXTURES = [
	{
		"id" : "1",
		"firstname" : "Maxime",
		"lastname" : "Grébauval",
		"number" : "0642472342",
		"status" : 1,
		"mail" : "maxime.grebauval@orange.com"
	},
	{
		"id" : "3",
		"firstname" : "Emmanuel",
		"lastname" : "Bertin",
		"number" : "+33231759410",
		"status" : 2,
		"mail" : "maxime.grebauval@orange.com"
	},
	{
		"id" : "4",
		"firstname" : "Antoine",
		"lastname" : "Bachelet",
		"number" : "+33231759108",
		"status" : 3,
		"mail" : "maxime.grebauval@orange.com"
	},
	{
		"id" : "2",
		"firstname" : "Stéphane",
		"lastname" : "Polouchkine",
		"number" : "+33231759416",
		"status" : 4,
		"mail" : "maxime.grebauval@orange.com"
	},
	{
		"id" : "5",
		"firstname" : "Marc",
		"lastname" : "Mazoue",
		"number" : "+33 231759274",
		"status" : 5,
		"mail" : "maxime.grebauval@orange.com"
	}
];