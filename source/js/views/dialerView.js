App.DialerView = Ember.View.create({
	click: function(event){
		var value = event.target.innerText;
		if(!value){
			value = event.target.id;
		}		
		this.get('controller').send('dialNumber', value);
	}
});